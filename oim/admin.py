from django.contrib import admin
from django.forms import Textarea 
from django.db import models
from oim.models import Tag, Location, Thesaurus, Tool, ToolFile

class FileInline(admin.TabularInline):
    model = ToolFile
    exclude = ('edited_by_signal', )
    readonly_fields = ['file_size']

class ToolAdmin(admin.ModelAdmin):
    list_display = ('product_title', 'product_name', 'project_frame', 'author', 'main_topic', 'population_approach', 'functional_category', 'year')
    inlines = [
        FileInline,
    ]
    search_fields = ['product_title', 'product_name', 'project_frame', 'author', 'main_topic']
    list_filter = ('year', 'population_approach', 'functional_category')
    exclude = ('tags', 'card_class', 'cover_class')
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows': 1, 'cols':40})},
    }

class LocationAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ['name']
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows': 1, 'cols':40})},
    }

class ThesaurusAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ['name']
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows': 1, 'cols':40})},
    }

# Register your models to admin site, then you can add, edit, delete and search your models in Django admin site.
admin.site.register(Location, LocationAdmin)
admin.site.register(Thesaurus, ThesaurusAdmin)
admin.site.register(Tool, ToolAdmin)