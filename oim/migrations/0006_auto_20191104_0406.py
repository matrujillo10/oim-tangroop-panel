# Generated by Django 2.2.6 on 2019-11-04 04:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('oim', '0005_auto_20191103_2300'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tool',
            name='functional_category',
            field=models.CharField(choices=[('Diagnóstico', 'Diagnóstico'), ('Herramientas Pedagógicas', 'Herramientas Pedagógicas'), ('Lineamientos de Política Pública', 'Lineamientos de Política Pública'), ('Metodologías', 'Metodologías'), ('Recopilación de Experiencias', 'Recopilación de Experiencias')], default='Multiple', max_length=40, verbose_name='Categoría Funcional'),
        ),
    ]
