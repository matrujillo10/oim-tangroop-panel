from django.db import models
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import CITextField
import datetime
import os

def year_choices():
    i = datetime.date.today().year+6
    result = []
    while i > 2009:
        result.append((i, i))
        i -= 1
    return result

def current_year():
    return datetime.date.today().year

tool_attrs_tags = [
    'product_title', 'product_name', 'description', 'main_topic', 'project_frame'
]


card_class_dict = {
    'Recursos diagnósticos': 'recursos_diagnosticos',
    'Recursos pedagógicos': 'recursos_pedagogicos',
    'Lineamientos de políticas públicas': 'recursos_lineamientos',
    'Recopilación de Experiencias': 'recursos_recopilacion_experiencias',
    'Recursos metodológicos': 'recursos_metodologias'
}

cover_class_dict = {
    'Recursos diagnósticos': 'div_diagnosticas',
    'Recursos pedagógicos': 'div_pedagogicas',
    'Lineamientos de políticas públicas': 'div_politica_publica',
    'Recopilación de Experiencias': 'div_recopilacion_experiencias',
    'Recursos metodológicos': 'div_metodologicas'
}

######################################################################################################
# Tag
######################################################################################################

class Tag(models.Model):
    name = CITextField('Nombre', max_length=2000, unique=True, null=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Etiqueta')
        verbose_name_plural = _('Etiquetas')

######################################################################################################
# Location
######################################################################################################

class Location(models.Model):
    name = CITextField('Nombre', max_length=255, unique=True, null=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Localización geográfica')
        verbose_name_plural = _('Localizaciones geográficas')

######################################################################################################
# Thesaurus
######################################################################################################

class Thesaurus(models.Model):
    name = CITextField('Nombre', max_length=2000, unique=True, null=False)

    def __str__(self):
        return os.path.basename(self.name)

    class Meta:
        verbose_name = _('Thesaurus')
        verbose_name_plural = _('Thesaurus')


######################################################################################################
# Tool
######################################################################################################

class Tool(models.Model):
    project_number = models.CharField('N° Proyecto', max_length=30, null=True, blank=True)
    folder = models.CharField('Carpeta', max_length=30, null=True, blank=True)
    product_title = models.TextField('Título', max_length=500, null=False, blank=False)
    product_name = models.TextField('Nombre Producto', max_length=500, null=False, blank=False)
    project_frame = models.TextField('Proyecto Marco', max_length=500, null=False, blank=False)
    author = models.TextField('Autor/es', max_length=500, null=False, blank=False)
    description = models.TextField('Descripción', max_length=2000, null=False, blank=False)
    main_topic = models.TextField('Tema Central', max_length=500, null=False, blank=False)
    population_approach = models.CharField('Enfoque poblacional', choices=[
        ('Étnico', 'Étnico'), ('Mujeres', 'Mujeres'), ('Niñez', 'Niñez'), 
        ('LGBTI', 'LGBTI'), ('Afrodescendientes', 'Afrodescendientes'), ('Discapacidad', 'Discapacidad'),
        ('Interseccional', 'Interseccional'), ('Multiple', 'Multiple'), ('Adulto Mayor', 'Adulto Mayor'),
        ('Juventud', 'Juventud')
    ], default='Multiple', max_length=30)
    functional_category = models.CharField('Categoría Funcional', choices=[
        ('Recursos diagnósticos', 'Recursos diagnósticos'), ('Recursos pedagógicos', 'Recursos pedagógicos'), 
        ('Lineamientos de políticas públicas', 'Lineamientos de políticas públicas'), 
        ('Recursos metodológicos', 'Recursos metodológicos'), ('Recopilación de Experiencias', 'Recopilación de Experiencias')
    ], default='Multiple', max_length=40)
    locations = models.ManyToManyField(Location, verbose_name='Localizaciones geográficas')
    thesaurus = models.ManyToManyField(Thesaurus, verbose_name='Thesaurus', blank=True)
    year = models.IntegerField(_('Año'), choices=year_choices(), default=current_year())

    tags = models.ManyToManyField(Tag)
    card_class = models.CharField(max_length=40, null=True)
    cover_class = models.CharField(max_length=40, null=True)

    # TODO: revisar este cual es: technical_title = models.TextField('Título técnico', max_length=255, null=False)

    def __str__(self):
        return (self.product_title)

    class Meta:
        verbose_name = _('Herramienta')
        verbose_name_plural = _('Herramientas')

######################################################################################################
# Helpers
######################################################################################################

def str_to_words(text):
    # split into words
    from nltk.tokenize import word_tokenize
    tokens = word_tokenize(text)
    # convert to lower case
    tokens = [w.lower() for w in tokens]
    # remove punctuation from each word
    import string
    table = str.maketrans('', '', string.punctuation)
    stripped = [w.translate(table) for w in tokens]
    # remove remaining tokens that are not alphabetic
    words = [word for word in stripped if word.isalpha()]
    # filter out stop words
    from nltk.corpus import stopwords
    stop_words = set(stopwords.words('spanish'))
    return [w for w in words if not w in stop_words]

######################################################################################################
# ToolFile
######################################################################################################

class ToolFile(models.Model):
    name = models.CharField('Nombre', max_length=100, unique=False, null=False)
    file_type = models.CharField('Tipo de Archivo', max_length=30, choices=[
        ('Documento (Texto/Lectura)', 'Documento (Texto/Lectura)'), ('Imagen', 'Imagen'), ('Video', 'Video'),
        ('Audio', 'Audio'), ('Comprimido', 'Comprimido'), ('Ejecutable', 'Ejecutable')
    ], null=False)
    file_size = models.CharField('Peso', max_length=20, null=True)
    tool_file = models.FileField('Herramienta', upload_to='tools/%Y/%m/%D/', null=True)
    tool = models.ForeignKey(Tool, on_delete=models.CASCADE)

    edited_by_signal = models.BooleanField(default=False)

    def __str__(self):
        return os.path.basename(self.name)

    class Meta:
        verbose_name = _('Archivo de herramienta')
        verbose_name_plural = _('Archivos de herramientas')

######################################################################################################
# ToolFile Signals
######################################################################################################

@receiver(models.signals.post_delete, sender=ToolFile)
def on_delete_file(sender, instance, **kwargs):
    if instance.tool_file:
        if os.path.isfile(instance.tool_file.path):
            os.remove(instance.tool_file.path)

@receiver(models.signals.pre_save, sender=ToolFile)
def on_change_file(sender, instance, **kwargs):
    if not instance.pk:
        return False

    try:
        old_file = ToolFile.objects.get(pk=instance.pk).tool_file
    except ToolFile.DoesNotExist:
        return False

    new_file = instance.tool_file
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)

@receiver(models.signals.post_save, sender=ToolFile)
def on_save_file(sender, instance, **kwargs):
    if (not instance.pk) or instance.edited_by_signal:
        ToolFile.objects.filter(pk=instance.pk).update(edited_by_signal=False)
        return False
    
    instance.edited_by_signal = True
    size = os.path.getsize(instance.tool_file.path)
    if size <= 2**9:
        instance.file_size = str(size) + ' B'
    elif size < 2**20:
        instance.file_size = str(size >> 10) + ' KB'
    elif size < 2**30:
        instance.file_size = str(size >> 20) + ' MB'
    elif size < 2**40:
        instance.file_size = str(size >> 30) + ' GB'
    else:
        instance.file_size = str(size >> 40) + ' TB'

    instance.save()

######################################################################################################
# Location Signals
######################################################################################################

@receiver(models.signals.post_delete, sender=Location)
def on_delete_location(sender, instance, **kwargs):
    try:
        Tag.objects.get(name=instance.name).delete()
    except:
        pass

@receiver(models.signals.pre_save, sender=Location)
def on_change_location(sender, instance, **kwargs):
    if not instance.pk and Location.objects.filter(name=instance.name).first() is not None:
        return False

    try:
        old_name = Location.objects.get(pk=instance.pk).name
    except Location.DoesNotExist:
        return False

    new_name = instance.name
    if old_name != new_name:
        Tag.objects.filter(pk=Tag.objects.get(name=old_name).pk).update(name=new_name)

######################################################################################################
# Thesaurus Signals
######################################################################################################

@receiver(models.signals.post_delete, sender=Thesaurus)
def on_delete_thesaurus(sender, instance, **kwargs):
    try:
        Tag.objects.get(name=instance.name).delete()
    except:
        pass

@receiver(models.signals.pre_save, sender=Thesaurus)
def on_change_thesaurus(sender, instance, **kwargs):
    if not instance.pk and Thesaurus.objects.filter(name=instance.name).first() is not None:
        return False

    try:
        old_name = Thesaurus.objects.get(pk=instance.pk).name
    except Thesaurus.DoesNotExist:
        return False

    new_name = instance.name
    if old_name != new_name:
        Tag.objects.filter(pk=Tag.objects.get(name=old_name).pk).update(name=new_name)

######################################################################################################
# Tool Signals
######################################################################################################

@receiver(models.signals.post_delete, sender=Tool)
def on_delete_tool(sender, instance, **kwargs):
    if not instance.pk:
        return False

    for attr in tool_attrs_tags:
        try:
            tag = Tag.objects.get(name=getattr(instance, attr))
            if tag.tool_set.count() == 1:
                tag.delete()
        except:
            print('Esto solo pasa en desarrollo debido al error de tags')

    for location in instance.locations.all():
        try:
            tag = Tag.objects.get(name=location.name)[0]
            if tag.tool_set.count() == 1:
                tag.delete()
        except:
            print('Esto solo pasa en desarrollo debido al error de tags')
    
    for thesaurus in instance.thesaurus.all():
        try:
            tag = Tag.objects.get(name=thesaurus.name)[0]
            if tag.tool_set.count() == 1:
                tag.delete()
        except:
            print('Esto solo pasa en desarrollo debido al error de tags')

@receiver(models.signals.pre_save, sender=Tool)
def on_change_tool(sender, instance, **kwargs):
    if not instance.pk:
        return False

    try:
        old_tool = Tool.objects.get(pk=instance.pk)
    except Tool.DoesNotExist:
        return False
        
    for attr in tool_attrs_tags:
        old_value = getattr(old_tool, attr)
        new_value = getattr(instance, attr)
        if new_value != old_value:
            # Remove all the old tags
            for word in str_to_words(old_value):
                old_tag = Tag.objects.get(name=word)
                if old_tag.tool_set.count() == 1:
                    old_tag.delete()
                else:
                    old_tag.tool_set.remove(old_tool)

            # Add the new tags
            for word in str_to_words(new_value):
                tag = Tag.objects.get_or_create(name=word)[0]
                if not tag in instance.tags.all():
                    instance.tags.add(tag)

@receiver(models.signals.post_save, sender=Tool)
def on_save_tool(sender, instance, **kwargs):
    Tool.objects.filter(pk=instance.pk).update(card_class=card_class_dict[instance.functional_category],
                                                cover_class=cover_class_dict[instance.functional_category])
    for attr in tool_attrs_tags:
        for word in str_to_words(getattr(instance, attr)):
            tag = Tag.objects.get_or_create(name=word)[0]
            if not tag in instance.tags.all():
                instance.tags.add(tag)

######################################################################################################
# M2M Tool Signals
######################################################################################################


@receiver(models.signals.m2m_changed, sender=Tool.locations.through)
def on_save_tool_locations(sender, **kwargs):
    action = kwargs.pop('action', None)
    instance = kwargs.pop('instance', None)
    pks = kwargs.pop('pk_set', None)
    if action == 'post_add' and instance is not None and pks is not None and len(pks) > 0:
        for pk in pks:
            location = Location.objects.get(pk=pk)
            tag = Tag.objects.get_or_create(name=location.name)[0]
            if not tag in instance.tags.all():
                instance.tags.add(tag)
    elif action == 'post_remove' and instance is not None and pks is not None and len(pks) > 0:
        for pk in pks:
            location = Location.objects.get(pk=pk)
            tag = Tag.objects.get(name=location.name)
            if tag in instance.tags.all():
                instance.tags.remove(tag)
                if tag.tool_set.count() == 1:
                    tag.delete()

@receiver(models.signals.m2m_changed, sender=Tool.thesaurus.through)
def on_save_tool_thesaurus(sender, **kwargs):
    action = kwargs.pop('action', None)
    instance = kwargs.pop('instance', None)
    pks = kwargs.pop('pk_set', None)
    if action == 'post_add' and instance is not None and pks is not None and len(pks) > 0:
        for pk in pks:
            thesaurus = Thesaurus.objects.get(pk=pk)
            tag = Tag.objects.get_or_create(name=thesaurus.name)[0]
            if not tag in instance.tags.all():
                instance.tags.add(tag)
    elif action == 'post_remove' and instance is not None and pks is not None and len(pks) > 0:
        for pk in pks:
            thesaurus = Thesaurus.objects.get(pk=pk)
            tag = Tag.objects.get(name=thesaurus.name)
            if tag in instance.tags.all():
                instance.tags.remove(tag)
                if tag.tool_set.count() == 1:
                    tag.delete()
